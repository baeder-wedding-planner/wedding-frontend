import { TextField, FormControl, InputLabel, Select, IconButton } from "@material-ui/core";
import { CloudUpload } from "@material-ui/icons";
import axios from "axios";
import { useRef, useState } from "react";

export default function AddEntity() {
    const dateInput = useRef(null);
    const locationInp = useRef(null);
    const nameInput = useRef(null);
    const budgetInput = useRef(null);
    const reasonInp = useRef(null);
    const amountInp = useRef(null);
    const [file, setFile] = useState();
    const weddingID = useRef(null);
    const [state, setState] = useState({ type: "wedding" });
    const handleChange = (event) => {
        const name = event.target.name;
        setState({ ...state, [name]: event.target.value });
    }
    async function addEntity() {
        if (state.type === 'wedding') {
            const wedding = {
                primaryKey: 0,
                weddingDate: dateInput.current.value,
                locale: locationInp.current.value,
                lname: nameInput.current.value,
                budget: budgetInput.current.value
            };
            try {
                await axios.post('http://34.150.199.56:3000/weddings', wedding);
                alert('Wedding Has Been Created.');
                dateInput.current.value = '';
                locationInp.current.value = '';
                nameInput.current.value = '';
                budgetInput.current.value = '';
            } catch (error) {
                alert('An Error Occured, Wedding not Added.')
            }
        } else {
            let attachURL = '';
            if (file !== null) {
                function toBase64(file) {
                    return new Promise((resolve, reject) => {
                      const reader = new FileReader();
                      reader.readAsDataURL(file);
                      reader.onload = () => {
                        let encoded = reader.result.toString().replace(/^data:(.*,)?/, '');
                        if ((encoded.length % 4) > 0) {
                          encoded += '='.repeat(4 - (encoded.length % 4));
                        }
                        resolve(encoded);
                      };
                      reader.onerror = error => reject(error);
                    });
                }
                if (file.size <= (1) ^ 7) {
                    const fileContent = await toBase64(file);
                    const response = await axios.post('https://us-east4-wedding-planner-service.cloudfunctions.net/store-expense-attachments/upload', { fileName: file.name, content: fileContent })
                    attachURL = response.data;
                }
            }
            const expense = {
                reason: reasonInp.current.value,
                amount: amountInp.current.value,
                attachment: attachURL.photoLink,
                foreignKey: weddingID.current.value
            };
            try {
                await axios.post('http://34.150.199.56:3000/expenses', expense);
                alert('Expense Has Been Created.');
                reasonInp.current.value = '';
                amountInp.current.value = '';
                weddingID.current.value = '';
            } catch (error) {
                alert('An Error Occured, Expense not Added.')
            }
        }
    }
    return (<form noValidate autoComplete="off">
        <FormControl variant="filled">
            <InputLabel htmlFor="filled-native-simple">Type</InputLabel>
            <Select
                native
                value={state.type}
                onChange={handleChange}
                inputProps={{
                    name: 'type',
                    id: 'filled-native-simple',
                }}
            >
                <option value="wedding">Wedding</option>
                <option value="expense">Expense</option>
            </Select>
        </FormControl>
        {state.type === 'expense' ? <TextField id="filled-helperText" label="Reason" variant="filled" required inputRef={reasonInp} /> : <TextField id="filled-helperText" label="Date Format: YYYY-MM-DD" variant="filled" required inputRef={dateInput} />}
        {state.type === 'expense' ? <TextField id="filled-helperText" label="Amount" type="number" required variant="filled" inputRef={amountInp} /> : <TextField id="filled-helperText" label="Location" variant="filled" required inputRef={locationInp} />}
        {state.type === 'expense' ? <TextField id="filled-helperText" label="Wedding ID" variant="filled" required inputRef={weddingID} /> : <TextField id="filled-helperText" label="Name" variant="filled" required inputRef={nameInput} />}
        {state.type === 'expense' ? <TextField accept="image/png, image/jpg" id="filled-helperText" InputLabelProps={{ shrink: true, }} label="Attachment (Optional)" type="file" onChange={(e) => setFile(e.target.files[0])} /> : <TextField id="filled-helperText" label="Budget" type="number" variant="filled" required inputRef={budgetInput} />}
        <IconButton aria-label="delete" onClick={addEntity}><CloudUpload fontSize="large" size="large" /></IconButton>
    </form>)
}