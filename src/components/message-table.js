import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@material-ui/core";

export default function MessageTable(props) {
    const messages = props.messages;
    let counter = -1;
    return (<TableContainer>
        <Table aria-label="simple table">
            <TableHead>
                <TableRow>
                    <TableCell align="center">Sender</TableCell>
                    <TableCell align="center">Recipient</TableCell>
                    <TableCell align="center">Message</TableCell>
                    <TableCell align="center">Timestamp</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {messages.map(m => (<TableRow key = {++counter}>
                        <TableCell component="th" scope="row">{m.sender}</TableCell>
                        <TableCell align="center">{m.recipient}</TableCell>
                        <TableCell align="center">{m.note}</TableCell>
                        <TableCell align="center">{m.timestamp}</TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    </TableContainer>)
}