import { FormControl, InputLabel, Select, IconButton, TextField } from "@material-ui/core";
import { useState, useRef } from "react";
import DeleteIcon from '@material-ui/icons/Delete';
import axios from "axios";

export default function RemoveEntity() {
    const idInput = useRef(null);
    const [state, setState] = useState({type:"wedding"});
    const handleChange = (event) => {
        const name = event.target.name;
        setState({...state,[name]: event.target.value});
    }
    async function deleteEntity(){
        const id = idInput.current.value;
        const type = state.type
        if (type === "wedding"){
            try {
                const response = await axios.delete(`http://34.150.199.56:3000/weddings/${id}`)
                alert(response.data);
                idInput.current.value = "";
            } catch (error) {
                alert(`Error Deleting Wedding with ID ${id}.`);
            }
        } else {
            try {
                const response = await axios.delete(`http://34.150.199.56:3000/expenses/${id}`)
                alert(response.data);
                idInput.current.value = "";
            } catch (error) {
                alert(`Error Deleting Expense with ID ${id}.`);
            }
        }
    }
    return (<div>
        <FormControl variant="filled">
            <InputLabel htmlFor="filled-native-simple">Type</InputLabel>
            <Select
                native
                value={state.type}
                onChange={handleChange}
                inputProps={{
                    name: 'type',
                    id: 'filled-native-simple',
                }}
            >
                <option value="wedding">Wedding</option>
                <option value="expense">Expense</option>
            </Select>
        </FormControl>
        {state.type === "expense" ? <TextField id="filled-helperText" label="Expense ID" variant="filled" inputRef={idInput}/> : <TextField id="filled-helperText" label="Wedding ID" variant="filled" inputRef={idInput}/>}
        <IconButton aria-label="delete"onClick={deleteEntity}><DeleteIcon fontSize="large"size="large"/></IconButton>
    </div>)
}