import React from 'react';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

export default function WeddingTable(props) {
    const wedding = props.wedding;
    const expenses = props.expenses;
    const [open, setOpen] = React.useState(false);
    function dateStr(str) {
        const ret = str.substring(0, str.indexOf("T"))
        return ret;
    }
    function getLink(str) {
        if (str === '') {
            return('');
        } else {
            return <a href={str}>Attachment</a>
        }
    }
    function makeExpenseDropdown() {
        if (expenses.length > 0) {
            return (
                <TableRow>
                    <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                        <Collapse in={open} timeout="auto" unmountOnExit>
                            <Box margin={1}>
                                <Typography variant="h6" gutterBottom component="div">
                                    Expenses
                                </Typography>
                                <Table size="small" aria-label="expenses">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell align="center">Expense ID</TableCell>
                                            <TableCell align="center">Reason</TableCell>
                                            <TableCell align="center">Amount</TableCell>
                                            <TableCell align="center">Attachment</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {expenses.map(e => <TableRow key={e.primaryKey}>
                                            <TableCell align="center" component="th" scope="row">{e.primaryKey}</TableCell>
                                            <TableCell align="center">{e.reason}</TableCell>
                                            <TableCell align="center">{e.amount}</TableCell>
                                            <TableCell align="center">{getLink(e.attachment)}</TableCell>
                                        </TableRow>)}
                                    </TableBody>
                                </Table>
                            </Box>
                        </Collapse>
                    </TableCell>
                </TableRow>
            )
        }
    }
    return (<>
        <TableRow>
            <TableCell>
                {expenses.length > 0
                    ?
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                    :
                    <IconButton aria-label="expand row" size="small"><CloseIcon></CloseIcon></IconButton>
                }
            </TableCell>
            <TableCell align="center" component="th" scope="row">{wedding.primaryKey}</TableCell>
            <TableCell align="center">{dateStr(wedding.weddingDate)}</TableCell>
            <TableCell align="center">{wedding.locale}</TableCell>
            <TableCell align="center">{wedding.lname}</TableCell>
            <TableCell align="center">{wedding.budget}</TableCell>
        </TableRow>
        {makeExpenseDropdown()}
    </>)
}