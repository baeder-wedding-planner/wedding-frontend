import { createStore } from "redux";

const reducer= (userState={user:{email: '', fname:'', lname:''}}, message)=>{
    if(message.type === "add"){
        const newState = {user:{email: message.user.email, fname:message.user.fname, lname:message.user.lname}};
        return newState;
    }
    if(message.type === "remove"){
        const emptyState = {user:{email: '', fname:'',lname:''}};
        return emptyState;
    }
    return userState;
}

let initialState = JSON.parse(window.localStorage.getItem("userinfo")) ?? {user:{email: '', fname:'', lname:''}};

const store = createStore(reducer, initialState);

store.subscribe(()=>{
    window.localStorage.setItem("userinfo",JSON.stringify(store.getState()))
})

export default store;