import { FormControl, IconButton, InputLabel, Select, TextField } from "@material-ui/core";
import UpdateIcon from '@material-ui/icons/Update';
import axios from "axios";
import { useRef, useState } from "react";

export default function UpdateEntity() {
    const idInput = useRef(null);
    const dateInput = useRef(null);
    const locationInp = useRef(null);
    const nameInput = useRef(null);
    const budgetInput = useRef(null);
    const reasonInp = useRef(null);
    const amountInp = useRef(null);
    const attachmentInp = useRef(null);
    const [state, setState] = useState({ type: "wedding" });
    const handleChange = (event) => {
        const name = event.target.name;
        setState({ ...state, [name]: event.target.value });
    }
    async function updateEntity() {
        const type = state.type;
        if (type === 'wedding') {
            const update = {weddingDate:'',locale:'',lname:'',budget:0};
            const getWed = await axios.get(`http://34.150.199.56:3000/weddings/${idInput.current.value}`);
            const wedding = getWed.data
            if (dateInput.current.value) {
                update.weddingDate = dateInput.current.value;
            } else {
                update.weddingDate = wedding.weddingDate;
            }
            if (locationInp.current.value) {
                update.locale = locationInp.current.value;
            } else {
                update.locale = wedding.weddingDate;
            }
            if (nameInput.current.value) {
                update.lname = nameInput.current.value;
            } else {
                update.lname = wedding.lname;
            }
            if (budgetInput.current.value) {
                update.budget = budgetInput.current.value
            } else {
                update.budget = wedding.budget;
            }
            try {
                await axios.put(`http://34.150.199.56:3000/weddings/${idInput.current.value}`,update)
                alert(`Wedding with ID ${idInput.current.value} Successfully Updated.`);
                idInput.current.value = null
                dateInput.current.value = null
                locationInp.current.value = null
                nameInput.current.value = null
                budgetInput.current.value = null
            } catch (error){
                alert('An Error Ocurred, Wedding not Updated.')
            }
        } else {
            const update = {reason:'',amount:0,attachment:''};
            const getExp = await axios.get(`http://34.150.199.56:3000/expenses/${idInput.current.value}`)
            const expense = getExp.data;
            if (reasonInp.current.value) {
                update.reason = reasonInp.current.value;
            } else {
                update.reason = expense.reason;
            }
            if (amountInp.current.value) {
                update.amount = amountInp.current.value;
            } else {
                update.amount = expense.amount;
            }
            if (attachmentInp.current.value) {
                update.attachment = attachmentInp.current.value;
            } else {
                update.attachment = expense.attachment;
            }
            try {
                await axios.put(`http://34.150.199.56:3000/expenses/${idInput.current.value}`,update)
                alert(`Expense with ID ${idInput.current.value} Successfully Updated.`);
                idInput.current.value = null
                reasonInp.current.value = null
                amountInp.current.value = null
                attachmentInp.current.value = null
            } catch (error) {
                alert('An Error Occured, Expense not Updated.')
            }
        }
    }
    return (<div>
        <FormControl variant="filled">
            <InputLabel htmlFor="filled-native-simple">Type</InputLabel>
            <Select
                native
                value={state.type}
                onChange={handleChange}
                inputProps={{
                    name: 'type',
                    id: 'filled-native-simple',
                }}
            >
                <option value="wedding">Wedding</option>
                <option value="expense">Expense</option>
            </Select>
        </FormControl>
        {state.type === "expense" ? <TextField id="filled-helperText" label="Expense ID" variant="filled" inputRef={idInput} /> : <TextField id="filled-helperText" label="Wedding ID" variant="filled" inputRef={idInput} />}
        {state.type === "expense" ? <TextField id="filled-helperText" label="Reason" variant="filled" inputRef={reasonInp} /> : <TextField id="filled-helperText" label="Date Format: YYYY-MM-DD" variant="filled" inputRef={dateInput} />}
        {state.type === "expense" ? <TextField id="filled-helperText" label="Amount" variant="filled" inputRef={amountInp} /> : <TextField id="filled-helperText" label="Location" variant="filled" inputRef={locationInp} />}
        {state.type === "expense" ? <TextField id="filled-helperText" label="Attachment" variant="filled" inputRef={attachmentInp} /> : <TextField id="filled-helperText" label="Name" variant="filled" inputRef={nameInput} />}
        {state.type === "expense" ? <></> : <TextField id="filled-helperText" label="Budget" variant="filled" inputRef={budgetInput} />}
        <IconButton aria-label="update" onClick={updateEntity}><UpdateIcon fontSize="large" size="large" /></IconButton>
    </div>)
}