import axios from "axios";
import { useState } from "react";
import { IconButton } from "@material-ui/core";
import BuildIcon from '@material-ui/icons/Build';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import ChatIcon from '@material-ui/icons/Chat';
import RemoveEntity from "./remove-entity";
import UpdateEntity from "./update-entity";
import CollapsibleTable from "./outer-table";
import AddEntity from "./add-form";
import { useSelector } from "react-redux";

export default function PlannerView() {
    const auth = useSelector((state) => state.user);
    if (auth.fname === '') {
        window.location.href = '/login'
    }
    const [weddings, setWeddings] = useState([]);
    const [expenses, setExpenses] = useState([]);
    async function getWeddingsAndExpenses() {
        const wedRes = await axios.get('http://34.150.199.56:3000/weddings')
        setWeddings(wedRes.data);
        const expRes = await axios.get('http://34.150.199.56:3000/expenses')
        setExpenses(expRes.data);
    }
    async function logOut() {
        window.localStorage.clear();
        window.location.href = '/login';
    }
    async function redirect() {
        window.location.href = '/message';
    }
    return (<div style={{ "textAlign": "center" }}>
        <form style={{"textAlign": "left","float":"left"}}>
            <IconButton aria-label="logout" onClick={logOut}><LockOpenIcon fontSize="large" size="large" />Logout</IconButton>
        </form>
        <form style={{"textAlign": "right","float":"right"}}>
            <IconButton aria-label="messaging" onClick={redirect}><ChatIcon fontSize="large" size="large" />Messaging</IconButton>
        </form>
        <h1><u>Wedding Planner Page</u></h1>
        <h2 style={{"color": "rgb(65,65,65)" }}>Add A Wedding or Expense (Fill Out All Boxes Marked With an Asterisk)</h2>
        <AddEntity />
        <h2 style={{"color": "rgb(65,65,65)" }}>Update a Wedding or Expense (Only Fill Out Boxes Needing Updated)</h2>
        <UpdateEntity />
        <h2 style={{"color": "rgb(221,16,16)" }}>Remove a Wedding or Expense</h2>
        <RemoveEntity />
        <br></br>
        <br></br>
        <IconButton aria-label="delete" onClick={getWeddingsAndExpenses}><BuildIcon fontSize="large" size="large" />Get Current Wedding List</IconButton>
        {weddings.length === 0 ? <></> : <CollapsibleTable weddings={weddings} expenses={expenses} />}
    </div>)
}