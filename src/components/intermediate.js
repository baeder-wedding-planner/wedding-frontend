import { IconButton } from "@material-ui/core";
import LockOpenIcon from '@material-ui/icons/LockOpen';
import ChatIcon from '@material-ui/icons/Chat';
import AssignmentIcon from '@material-ui/icons/Assignment';
import { useSelector } from "react-redux";

export default function Interm() {
    const auth = useSelector((state) => state.user);
    if (auth.fname === '') {
        window.location.href = '/login'
    }
    async function logOut() {
        window.localStorage.clear();
        window.location.href = '/login';
    }
    async function redirectMessage() {
        window.location.href = '/message';
    }
    async function redirectPlanner() {
        window.location.href = '/planner';
    }
    return (<div style={{ "textAlign": "center" }}>
        <IconButton style={{"float":"left"}}aria-label="logout" onClick={logOut}><LockOpenIcon fontSize="large" size="large" />Logout</IconButton>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <IconButton aria-label="planner" onClick={redirectPlanner}><AssignmentIcon fontSize="large" size="large" />Planner</IconButton>
        <IconButton aria-label="messaging" onClick={redirectMessage}><ChatIcon fontSize="large" size="large" />Messaging</IconButton>
    </div>)
}