import { IconButton, TextField } from "@material-ui/core";
import BuildIcon from '@material-ui/icons/Build';
import ChatIcon from '@material-ui/icons/Chat';
import AssignmentIcon from '@material-ui/icons/Assignment';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import axios from "axios";
import { useRef, useState } from "react";
import { useSelector } from "react-redux";
import MessageTable from "./message-table";

export default function MessageView() {
    const auth = useSelector((state) => state.user);
    if (auth.fname === '') {
        window.location.href = '/login'
    }
    const [messages, setMessages] = useState([]);
    const recipInp = useRef('');
    const noteInp = useRef('');
    async function populateMessaging() {
        const resSent = await axios.get(`https://wedding-planner-service.uk.r.appspot.com/messages?sender=${auth.email}`);
        const resRec = await axios.get(`https://wedding-planner-service.uk.r.appspot.com/messages?recipient=${auth.email}`);
        const sent = resSent.data;
        const recieved = resRec.data;
        const allMessages = sent.concat(recieved);
        function compare( a, b ) {
            if ( a.timestamp < b.timestamp ){
              return -1;
            }
            if ( a.timestamp > b.timestamp ){
              return 1;
            }
            return 0;
        }
        allMessages.sort( compare );
        setMessages(allMessages);
    }
    async function sendMessage() {
        try {
            const recip = recipInp.current.value;
            const message = noteInp.current.value;
            await axios.get(`https://wedding-planner-service.uk.r.appspot.com/users/${recip}/verify`);
            await axios.post('https://wedding-planner-service.uk.r.appspot.com/messages', { sender: auth.email, recipient: recip, note: message });
            recipInp.current.value = '';
            noteInp.current.value = '';
            alert('Message has been sent.');
        } catch (error) {
            alert('Message is being sent to an invalid email.')
        }
    }
    async function logOut() {
        window.localStorage.clear();
        window.location.href = '/login';
    }
    async function redirect() {
        window.location.href = '/planner';
    }
    return (<div style={{ "textAlign": "center" }}>
        <form style={{ "textAlign": "left", "float": "left" }}>
            <IconButton aria-label="logout" onClick={logOut}><LockOpenIcon fontSize="large" size="large" />Logout</IconButton>
        </form>
        <form style={{ "textAlign": "right", "float": "right" }}>
            <IconButton aria-label="planner" onClick={redirect}><AssignmentIcon fontSize="large" size="large" />Planner</IconButton>
        </form>
        <h1>Employee Messaging Center</h1>
        <h2>Send a New Message</h2>
        <form>
            <TextField id="filled-helperText"label="Recipient"variant="filled" required inputRef={recipInp}/>
            <TextField id="filled-helperText"label="Message"variant="filled" required inputRef={noteInp}/>
            <IconButton aria-label="messaging" onClick={sendMessage}><ChatIcon fontSize="large" size="large" />Send</IconButton>
        </form>
        <br></br>
        <IconButton aria-label="delete" onClick={populateMessaging}><BuildIcon fontSize="large" size="large" />Get Your Current Message Log</IconButton>
        {messages.length === 0 ? <></> : <MessageTable messages={messages} />}
    </div>)
}