import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { TableContainer } from '@material-ui/core';
import WeddingTable from './table-content'

export default function CollapsibleTable(props) {
  const weddings = props.weddings
  const expenses = props.expenses
  function getExpenses(foreignKey) {
    const newArr = []
    for (let e of expenses) {
      if (e.foreignKey === foreignKey) {
        newArr.push(e);
      }
    }
    return newArr;
  }
  return (
    <TableContainer>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <TableCell></TableCell>
            <TableCell align="center">Wedding ID</TableCell>
            <TableCell align="center">Wedding Date</TableCell>
            <TableCell align="center">Wedding Location</TableCell>
            <TableCell align="center">Wedding Name</TableCell>
            <TableCell align="center">Wedding Budget</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {weddings.map(w => <WeddingTable key={w.primaryKey}wedding={w}expenses={getExpenses(w.primaryKey)}/>)}
        </TableBody>
      </Table>
    </TableContainer>
  );
}