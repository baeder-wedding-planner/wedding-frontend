import axios from "axios";
import { useRef } from "react";
import { TextField, IconButton } from "@material-ui/core";
import LockOpenIcon from '@material-ui/icons/LockOpen';
import { useDispatch } from "react-redux";


export default function AuthorizationView() {
    const emailInp = useRef('');
    const passInp = useRef('');
    const dispatch = useDispatch();
    async function login() {
        const em = emailInp.current.value;
        const pass = passInp.current.value;
        try{
            await axios.get(`https://wedding-planner-service.uk.r.appspot.com/users/${em}/verify`);
            const res = await axios.patch(`https://wedding-planner-service.uk.r.appspot.com/users/login`,{email:em, password:pass});
            const info = res.data;
            const user = {email:em, fname:info.fname, lname:info.lname};
            dispatch({type:"add",user});
            emailInp.current.value = '';
            passInp.current.value = '';
            window.location.href = '/select'
        } catch (error) {
            alert("Invalid Username or Password.");
        }
    }
    return (<form style={{ "textAlign": "center" }}>
        <h1>Employee Login</h1>
        <TextField id="filled-helperText" label="Email Address" variant="filled" required inputRef={emailInp} />
        <TextField id="password-helperText" label="Password" variant="filled" type="password" required inputRef={passInp} />
        <IconButton aria-label="delete" onClick={login}><LockOpenIcon fontSize="large" size="large" /></IconButton>
    </form>)
}