import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import AuthorizationView from './components/auth-view';
import Interm from './components/intermediate';
import MessageView from './components/messaging-view';
import PlannerView from './components/planner-view';
import store from './components/store/authenticate';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Router>
        <Switch>
          <Route path="/login"><AuthorizationView /></Route>
          <Route path="/select"><Interm /></Route>
          <Route path="/message"><MessageView /></Route>
          <Route path="/planner"><PlannerView /></Route>
        </Switch>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
